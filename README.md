# {chicopa}

{chicopa} is an R package that interfaces with Chicago Civilian Office of Police Accountability APIs. In order to use this R package, you will need to create an app token. You can create an app token by signing up with the [City of Chicago data portal](https://data.cityofchicago.org/profile/edit/developer_settings). Once you have signed up, navigate to Developer Settings and click "Create New App Token".

## Installation

You can install the latest release of chicopa from [GitLab](https://gitlab.com/jrwinget/chicopa) with:

``` r
library(remotes)
install_gitlab("jrwinget/chicopa")
```

## App token

The functions for the APIs have an `app_token` argument that defaults to an object called `chicopa_token`. By assigning the token to an object called `chicopa_token`, the `app_token` argument can be left blank in the functions:

``` r
chicopa_token <- xxxxxxxxxxxxx
get_summary()
```

However, the best approach is store the token in the `.Renviron` file. Doing so will automatically set the token as an environment variable on startup. The `usethis` package makes this method quite easy. First, edit the R environment file:

``` r
usethis::edit_r_environ()
```

Then, add a line in the format below but substitute all of the Xs with your app token:

``` r
chicopa_token = "xxxxxxxxxxxxx"
```

Finally, save the file and restart the R session. Now, you can use the functions without the need to manually specify the token:

``` r
get_summary()
```
